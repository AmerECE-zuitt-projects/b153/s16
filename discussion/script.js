function displayMsgToSelf () {
    console.log("Dont text back");
};

let count = 10;

while (count !== 0) {
    displayMsgToSelf();
    count--;
};

// Mini-activity
let i = 0
while (i <= 5) {
    console.log(i);
    i++;
};

// Do-while loop
/*
    it is similar to the while loop,
    the while loop will allows us to run our loop at least once
    - the while loop, we check the condition first before running the code.
    - for the do while loop, it will do an instruction first before it will check the condition to run again/
*/
let doWhileCounter = 1;
do {
    console.log(doWhileCounter);
} while (doWhileCounter === 0) {
    console.log(doWhileCounter);
    doWhileCounter--;
};

// Mini-Activity
let myCounter = 1;
do {
    console.log(myCounter);
    myCounter++;
} while (myCounter < 21);

// for-loop
/*
    for loops are much more flexible than the while-loop
    - it consist of three parts
        1. initialization - creating variable which can be used as the counter.
        2. condition - creating the appropriate condition to run the loop.
        3. finalExpression
*/
for (let count = 0; count <= 20; count++) {
    console.log(count);
};

let fruits = [
    "Apple",
    "Durian",
    "Kiwi",
    "Pineapple",
    "Strawberry"
];
console.log(fruits[2]);

console.log(fruits[fruits.length - 1]);

for (index = 0; index < fruits.length; index++) {
    console.log(fruits[index]);
};

// assign a new value
fruits[5] = "Grapes"

// Mini-Activity
let myArray = [
    "Yemen",
    "Iraq",
    "Canada",
    "Sweden",
    "Italy",
    "France"
];

console.log(myArray.slice(0, -2));

// Array of objects
let cars = [
    {
        brand: "Toyota",
        type: "Sedan"
    },
    {
        brand: "Rolls Royce",
        type: "Luxury Sedan"
    },
    {
        brand: "Mazda",
        type: "Hatchback"
    }
];
console.log(cars[1]);

// Mini-Activity
let MyName = "adrIAn madArang";

for (let x = 0; x < MyName.length; x++) {
    (MyName[x].toLowerCase() === "e" ||
    MyName[x].toLowerCase() === "a" ||
    MyName[x].toLowerCase() === "o" ||
    MyName[x].toLowerCase() === "u" ||
    MyName[x].toLowerCase() === "i") ? console.log(3) 
    : console.log(MyName[x]);
};


for (let a = 20; a > 0; a--) {
    if (a % 2 === 0) {
        continue
    };
    console.log(a);
}

// Mini-Activity
let string = "alejandro";

for (let i = 0; i < string.length; i++) {
    if (string[i].toLowerCase() === "a") {
        console.log("Continue to the next iteration.");
        continue;
    };
    if (string[i].toLowerCase() === "d") {
        break;
    };
    console.log(string[i]);
};